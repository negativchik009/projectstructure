using AutoMapper;
using BSA_N3.DataAccess.Abstractions;
using BSA_N3.DataAccess.Models;
using BSA_N3.DataAccess.Services;
using BSA_N3.WebApi.DTO;
using BSA_N3.WebApi.Interfaces;
using BSA_N3.WebApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace BSA_N3.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            
            var mapper = MapperConfiguration().CreateMapper();
            services.AddSingleton(_ => mapper);

            services.AddSingleton<IRepository<Project>, Repository<Project>>();
            services.AddSingleton<IRepository<Task>, Repository<Task>>();
            services.AddSingleton<IRepository<Team>, Repository<Team>>();
            services.AddSingleton<IRepository<User>, Repository<User>>();

            services.AddSingleton<IDataProcessor, DataProcessor>();
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "BSA_N3.WebApi", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BSA_N3.WebApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                
                cfg.CreateMap<Project, ProjectDto>();
                cfg.CreateMap<ProjectDto, Project>();

                cfg.CreateMap<Task, TaskDto>();
                cfg.CreateMap<TaskDto, Task>();

                cfg.CreateMap<Team, TeamDto>();
                cfg.CreateMap<TeamDto, Team>();

                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<UserDto, User>();
            });
            return config;
        }
    }
}