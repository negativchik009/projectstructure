﻿namespace BSA_N3.WebApi.DTO
{
    public class TaskIdNamePair
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}