﻿using System.Collections.Generic;

namespace BSA_N3.WebApi.DTO
{
    public class TeamInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDto> Members { get; set; }
    }
}