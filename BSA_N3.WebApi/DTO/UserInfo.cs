﻿namespace BSA_N3.WebApi.DTO
{
    public class UserInfo
    {
        public UserDto User { get; set; }
        public ProjectDto LastProject { get; set; }

        public int LastProjectTasksNumber { get; set; }

        public int UnfinishedTasksNumber { get; set; }

        public TaskDto LongestTask { get; set; }
    }
}