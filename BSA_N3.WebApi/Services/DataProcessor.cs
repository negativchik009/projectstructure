﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BSA_N3.DataAccess.Abstractions;
using BSA_N3.DataAccess.Models;
using BSA_N3.WebApi.DTO;
using BSA_N3.WebApi.Interfaces;

namespace BSA_N3.WebApi.Services
{
    public class DataProcessor : IDataProcessor
    {
        private readonly IMapper _mapper;

        private readonly IRepository<Project> _projects;
        private readonly IRepository<Task> _tasks;
        private readonly IRepository<Team> _teams;
        private readonly IRepository<User> _users;

        public  IEnumerable<ProjectDto> GetProjects(Func<Project, bool> filter = null) => 
            _mapper.Map<IEnumerable<Project>, IEnumerable<ProjectDto>>(_projects.Get(filter));

        public void Create(ProjectDto projectDto) => 
            _projects.Create(_mapper.Map<ProjectDto, Project>(projectDto));

        public void Update(ProjectDto projectDto) => 
            _projects.Update(_mapper.Map<ProjectDto, Project>(projectDto));

        public void Delete(ProjectDto projectDto) => 
            _projects.Delete(_mapper.Map<ProjectDto, Project>(projectDto));

        public IEnumerable<TaskDto> GetTasks(Func<Task, bool> filter = null) => 
            _mapper.Map<IEnumerable<Task>, IEnumerable<TaskDto>>(_tasks.Get(filter));

        public void Create(TaskDto taskDto) => 
            _tasks.Create(_mapper.Map<TaskDto, Task>(taskDto));

        public void Update(TaskDto taskDto) => 
            _tasks.Update(_mapper.Map<TaskDto, Task>(taskDto));

        public void Delete(TaskDto taskDto) => 
            _tasks.Delete(_mapper.Map<TaskDto, Task>(taskDto));

        public IEnumerable<TeamDto> GetTeams(Func<Team, bool> filter = null) => 
            _mapper.Map<IEnumerable<Team>, IEnumerable<TeamDto>>(_teams.Get(filter));

        public void Create(TeamDto teamDto) => 
            _teams.Create(_mapper.Map<TeamDto, Team>(teamDto));

        public void Update(TeamDto teamDto) => 
            _teams.Update(_mapper.Map<TeamDto, Team>(teamDto));

        public void Delete(TeamDto teamDto) => 
            _teams.Delete(_mapper.Map<TeamDto, Team>(teamDto));

        public IEnumerable<UserDto> GetUsers(Func<User, bool> filter = null) => 
            _mapper.Map<IEnumerable<User>, IEnumerable<UserDto>>(_users.Get(filter));

        public void Create(UserDto userDto) => 
            _users.Create(_mapper.Map<UserDto, User>(userDto));

        public void Update(UserDto userDto) => 
            _users.Update(_mapper.Map<UserDto, User>(userDto));

        public void Delete(UserDto userDto) => 
            _users.Delete(_mapper.Map<UserDto, User>(userDto));

        public DataProcessor(IRepository<Project> projects, IRepository<Task> tasks, IRepository<Team> teams, IRepository<User> users, IMapper mapper)
        {
            _projects = projects;
            _tasks = tasks;
            _teams = teams;
            _users = users;
            _mapper = mapper;
        }

        public Dictionary<ProjectDto, int> TasksForUserByProject(int userId)
        {
            return GetProjects().ToDictionary(project => project,
                project => 
                    _tasks.Get(task => task.PerformerId == userId) // Do not mapping for productivity
                        .Count(x => x.ProjectId == project.Id));
        }

        public IEnumerable<TaskDto> TasksByUserWithShortNames(int userId)
        {
            const int maxLenght = 45;
            return GetTasks(task => task.PerformerId == userId && task.Name.Length < maxLenght);
        }

        public IEnumerable<TaskIdNamePair> TasksFinishedInCurrentYear(int userId)
        {
            int currentYear = DateTime.Now.Year;
            return _tasks.Get(task => task.FinishedAt?.Year == currentYear)
                .Select(task => new TaskIdNamePair
                {
                    Id = task.Id,
                    Name = task.Name
                });
        }

        public IEnumerable<TeamInfo> TeamInfos()
        {
            const int minAge = 10;
            return GetTeams(team => _users
                    .Get(user => user.TeamId == team.Id)
                    .All(user => DateTime.Now.Year - user.BirthDay?.Year > minAge))
                .Select(team => new TeamInfo()
                {
                    Id = team.Id,
                    Name = team.Name,
                    Members = GetUsers(user => user.TeamId == team.Id)
                        .OrderByDescending(x => x.RegisteredAt)
                })
                .ToList();
        }

        public IEnumerable<UserWithTasks> SortedUsers()
        {
            return GetUsers().OrderBy(user => user.FirstName)
                .Select(user => new UserWithTasks()
                {
                    User = user,
                    Tasks = GetTasks().OrderByDescending(task => task.Name.Length)
                });
        }

        public UserInfo UserInfo(int userId)
        {
            return GetUsers(user => user.Id == userId)
                .Select(user => new UserInfo()
                {
                    User = user,
                    LastProject = _mapper.Map<Project, ProjectDto>(_projects
                        .Get(project => project.AuthorId == userId)
                        // Alternative with O(n) complexity:
                        // .Aggregate((latest, current) => current.CreatedAt > latest.CreatedAt ? current : latest)) 
                        .OrderBy(project => project.CreatedAt)
                        .Last()),
                    LongestTask = _mapper.Map<Task, TaskDto>(_tasks
                        .Get(task => task.PerformerId == userId)
                        // Alternative with O(n) complexity:
                        // .Aggregate((longest, current) => 
                        // (current.FinishedAt ?? DateTime.Now) - current.CreatedAt > (longest.FinishedAt ?? DateTime.Now) - longest.CreatedAt ? current : longest) 
                        .OrderBy(task => (task.FinishedAt ?? DateTime.Now) - task.CreatedAt).Last()),
                    LastProjectTasksNumber = _tasks.Get(task => task.ProjectId == _projects
                        .Get(project => project.AuthorId == userId)
                        // Alternative with O(n) complexity:
                        // .Aggregate((latest, current) => current.CreatedAt > latest.CreatedAt ? current : latest)) 
                        .OrderBy(project => project.CreatedAt)
                        .Last().Id)
                        .Count()
                })
                .First();
        }

        public IEnumerable<ProjectInfo> ProjectInfos()
        {
            return GetProjects().Select(project => new ProjectInfo()
            {
                Project = project,
                LongestByDescription = GetTasks(task => task.ProjectId == project.Id)
                    // Alternative with O(n) complexity:
                    // .Aggregate((longest, task) => task.Description.Length > longest.Description.Length ? task : longest)
                    .OrderBy(task => task.Description.Length)
                    .Last(),
                ShortestByName = GetTasks(task => task.ProjectId == project.Id)
                    // Alternative with O(n) complexity:
                    // .Aggregate((shortest, task) => task.Name.Length < shortest.Name.Length ? task : shortest)
                    .OrderBy(task => task.Name)
                    .First(),
                TeamUsersCount = GetTeams(team => team.Id == project.TeamId).Count()
            });
        }
    }
}