﻿using System;
using System.Collections.Generic;
using BSA_N3.DataAccess.Abstractions;
using BSA_N3.DataAccess.Models;
using BSA_N3.WebApi.DTO;

namespace BSA_N3.WebApi.Interfaces
{
    public interface IDataSource
    {
        public IEnumerable<ProjectDto> GetProjects(Func<Project, bool> filter = null);
        public void Create(ProjectDto projectDto);
        public void Update(ProjectDto projectDto);
        public void Delete(ProjectDto projectDto);

        public IEnumerable<TaskDto> GetTasks(Func<Task, bool> filter = null);
        public void Create(TaskDto taskDto);
        public void Update(TaskDto taskDto);
        public void Delete(TaskDto taskDto);

        public IEnumerable<TeamDto> GetTeams(Func<Team, bool> filter = null);
        public void Create(TeamDto teamDto);
        public void Update(TeamDto teamDto);
        public void Delete(TeamDto teamDto);

        public IEnumerable<UserDto> GetUsers(Func<User, bool> filter = null);
        public void Create(UserDto userDto);
        public void Update(UserDto userDto);
        public void Delete(UserDto userDto);
    }
}