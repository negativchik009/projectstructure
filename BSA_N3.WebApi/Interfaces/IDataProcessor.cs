﻿using System.Collections.Generic;
using BSA_N3.WebApi.DTO;

namespace BSA_N3.WebApi.Interfaces
{
    public interface IDataProcessor : IDataSource
    {
        public Dictionary<ProjectDto, int> TasksForUserByProject(int userId); // selection 1
        public IEnumerable<TaskDto> TasksByUserWithShortNames(int userId); // selection 2
        public IEnumerable<TaskIdNamePair> TasksFinishedInCurrentYear(int userId); // selection 3
        public IEnumerable<TeamInfo> TeamInfos(); // selection 4
        public IEnumerable<UserWithTasks> SortedUsers(); // selection 5
        public UserInfo UserInfo(int userId); // selection 6
        public IEnumerable<ProjectInfo> ProjectInfos(); // selection 7
    }
}