﻿using System.Collections.Generic;
using System.Linq;
using BSA_N3.WebApi.DTO;
using BSA_N3.WebApi.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BSA_N3.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly IDataProcessor _data;

        public TaskController(IDataProcessor data)
        {
            _data = data;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDto>> Get()
        {
            return new ActionResult<IEnumerable<TaskDto>>(_data.GetTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDto> Get(int id)
        {
            var taskDto = _data.GetTasks().FirstOrDefault(task => task.Id == id);
            return taskDto == null ? BadRequest($"Not found task with id {id}") 
                : new ActionResult<TaskDto>(taskDto);
        }

        [HttpPost]
        public ActionResult Post(TaskDto dto)
        {
            _data.Create(dto);
            return StatusCode(201);
        }

        [HttpPut]
        public ActionResult Put(TaskDto dto)
        {
            _data.Update(dto);
            return StatusCode(200);
        }

        [HttpDelete]
        public ActionResult Delete(TaskDto dto)
        {
            _data.Delete(dto);
            return StatusCode(204);
        }
    }
}