﻿using System.Collections.Generic;
using BSA_N3.WebApi.DTO;
using BSA_N3.WebApi.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BSA_N3.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpecialController : ControllerBase
    {
        private readonly IDataProcessor _data;

        public SpecialController(IDataProcessor data)
        {
            _data = data;
        }
        
        [HttpGet("/TasksForUserByProject/{userId}")]
        public ActionResult<Dictionary<ProjectDto, int>> TasksForUserByProject(int userId)
        {
            return new ActionResult<Dictionary<ProjectDto, int>>(_data.TasksForUserByProject(userId));
        }

        [HttpGet("/TasksByUserWithShortNames/{userId}")]
        public ActionResult<IEnumerable<TaskDto>> TasksByUserWithShortNames(int userId)
        {
            return new ActionResult<IEnumerable<TaskDto>>(_data.TasksByUserWithShortNames(userId));
        }

        [HttpGet("/TasksFinishedInCurrentYear/{userId}")]
        public ActionResult<IEnumerable<TaskIdNamePair>> TasksFinishedInCurrentYear(int userId)
        {
            return new ActionResult<IEnumerable<TaskIdNamePair>>(_data.TasksFinishedInCurrentYear(userId));
        }

        [HttpGet("/TeamInfos")]
        public ActionResult<IEnumerable<TeamInfo>> TeamInfos()
        {
            return new ActionResult<IEnumerable<TeamInfo>>(_data.TeamInfos());
        }

        [HttpGet("/SortedUsers")]
        public ActionResult<IEnumerable<UserWithTasks>> SortedUsers()
        {
            return new ActionResult<IEnumerable<UserWithTasks>>(_data.SortedUsers());
        }

        [HttpGet("/UserInfo/{userId}")]
        public ActionResult<UserInfo> UserInfo(int userId)
        {
            return new ActionResult<UserInfo>(_data.UserInfo(userId));
        }

        [HttpGet("/ProjectInfos")]
        public ActionResult<IEnumerable<ProjectInfo>> ProjectInfos()
        {
            return new ActionResult<IEnumerable<ProjectInfo>>(_data.ProjectInfos());
        }
    }
}