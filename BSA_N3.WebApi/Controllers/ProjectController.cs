﻿using System;
using System.Collections.Generic;
using System.Linq;
using BSA_N3.WebApi.DTO;
using BSA_N3.WebApi.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BSA_N3.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IDataProcessor _data;

        public ProjectController(IDataProcessor data)
        {
            _data = data;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDto>> Get()
        {
            return new ActionResult<IEnumerable<ProjectDto>>(_data.GetProjects());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDto> Get(int id)
        {
            var projectDto = _data.GetProjects().FirstOrDefault(project => project.Id == id);
            return projectDto == null ? BadRequest($"Not found project with id {id}") 
                : new ActionResult<ProjectDto>(projectDto);
        }

        [HttpPost]
        public ActionResult Post(ProjectDto dto)
        {
            _data.Create(dto);
            return StatusCode(201);
        }

        [HttpPut]
        public ActionResult Put(ProjectDto dto)
        {
            _data.Update(dto);
            return StatusCode(200);
        }

        [HttpDelete]
        public ActionResult Delete(ProjectDto dto)
        {
            _data.Delete(dto);
            return StatusCode(204);
        }
    }
}