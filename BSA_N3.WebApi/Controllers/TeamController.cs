﻿using System.Collections.Generic;
using System.Linq;
using BSA_N3.WebApi.DTO;
using BSA_N3.WebApi.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BSA_N3.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly IDataProcessor _data;

        public TeamController(IDataProcessor data)
        {
            _data = data;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDto>> Get()
        {
            return new ActionResult<IEnumerable<TeamDto>>(_data.GetTeams());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDto> Get(int id)
        {
            var teamDto = _data.GetTeams().FirstOrDefault(team => team.Id == id);
            return teamDto == null ? BadRequest($"Not found teams with id {id}") 
                : new ActionResult<TeamDto>(teamDto);
        }

        [HttpPost]
        public ActionResult Post(TeamDto dto)
        {
            _data.Create(dto);
            return StatusCode(201);
        }

        [HttpPut]
        public ActionResult Put(TeamDto dto)
        {
            _data.Update(dto);
            return StatusCode(200);
        }

        [HttpDelete]
        public ActionResult Delete(TeamDto dto)
        {
            _data.Delete(dto);
            return StatusCode(204);
        }
    }
}