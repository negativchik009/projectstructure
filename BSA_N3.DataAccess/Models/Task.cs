﻿using System;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.DataAccess.Models
{
    public class Task : EntityBase
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}