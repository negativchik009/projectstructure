﻿using System;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.DataAccess.Models
{
    public class Project : EntityBase
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}