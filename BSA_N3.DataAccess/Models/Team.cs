﻿using System;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.DataAccess.Models
{
    public class Team : EntityBase
    {
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}