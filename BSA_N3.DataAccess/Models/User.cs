﻿using System;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.DataAccess.Models
{
    public class User : EntityBase
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? RegisteredAt { get; set; }
        public DateTime? BirthDay { get; set; }
    }
}