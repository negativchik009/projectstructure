﻿namespace BSA_N3.DataAccess.Models
{
    public enum TaskState : byte
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}