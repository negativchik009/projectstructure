﻿namespace BSA_N3.DataAccess.Abstractions
{
    public abstract class EntityBase
    {
        public int Id { get; set; }
    }
}