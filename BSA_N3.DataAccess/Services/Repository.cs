﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.DataAccess.Services
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        private readonly List<TEntity> _collection;

        public Repository()
        {
            _collection = new List<TEntity>();
        }

        public void Create(TEntity entity)
        {
            _collection.Add(entity);
        }

        public IEnumerable<TEntity> Get(Func<TEntity, bool> filter = null)
        {
            return filter == null ? _collection : _collection.Where(filter).ToList();
        }

        public void Update(TEntity entity)
        {
            int index = _collection.FindIndex(x => x.Id == entity.Id);
            if (index == -1)
            {
                throw new ArgumentException("Element not found");
            }

            _collection[index] = entity;
        }

        public void Delete(TEntity entity)
        {
            _collection.RemoveAll(x => entity.Id == x.Id);
        }
    }
}