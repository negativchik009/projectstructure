﻿namespace BSA_N3.Client.Models
{
    public class TaskIdNamePair
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}