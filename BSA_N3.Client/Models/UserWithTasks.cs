﻿using System.Collections.Generic;

namespace BSA_N3.Client.Models
{
    public class UserWithTasks
    {
        public User User { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
    }
}