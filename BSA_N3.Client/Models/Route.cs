﻿namespace BSA_N3.Client.Models
{
    public enum Route : byte
    {
        Projects,
        Tasks,
        Teams,
        Users,
        Selection1,
        Selection2,
        Selection3,
        Selection4,
        Selection5,
        Selection6,
        Selection7
    }
}