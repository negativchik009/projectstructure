﻿using System.Linq;
using System.Reflection.Metadata;

namespace BSA_N3.Client.Models
{
    public class ProjectInfo
    {
        public Project Project { get; set; }

        public Task LongestByDescription { get; set; }

        public Task ShortestByName { get; set; }

        public int TeamUsersCount { get; set; }
    }
}