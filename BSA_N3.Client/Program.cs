﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using BSA_N3.Client.Models;
using Newtonsoft.Json;
using Task = System.Threading.Tasks.Task;

namespace BSA_N3.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var processor = new HttpDataGetter(new Dictionary<Route, string>
            {
                {Route.Projects, "https://localhost:5001/api/Project"},
                {Route.Tasks, "https://bsa21.azurewebsites.net/api/Task"},
                {Route.Teams, "https://bsa21.azurewebsites.net/api/Team"},
                {Route.Users, "https://bsa21.azurewebsites.net/api/User"},
                {Route.Selection1, "https://bsa21.azurewebsites.net/api/Special/TasksForUserByProject/"},
                {Route.Selection2, "https://bsa21.azurewebsites.net/api/Special/TasksByUserWithShortNames/"},
                {Route.Selection3, "https://bsa21.azurewebsites.net/api/Special/TasksFinishedInCurrentYear/"},
                {Route.Selection4, "https://bsa21.azurewebsites.net/api/Special/TeamInfos"},
                {Route.Selection5, "https://bsa21.azurewebsites.net/api/Special/SortedUsers"},
                {Route.Selection6, "https://bsa21.azurewebsites.net/api/Special/UserInfo/"},
                {Route.Selection7, "https://bsa21.azurewebsites.net/api/Special/ProjectInfos"},
            }, new HttpClient());
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("First request: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Enter value of UserId");
            int buffer = Convert.ToInt32(Console.ReadLine());
            Console.ForegroundColor = default;
            foreach ((var key, int value) in await processor.TasksForUserByProject(buffer))
            {
                Console.WriteLine($"{key.Name} : {value}");
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Second request: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Enter value of UserId");
            Console.ForegroundColor = default;
            buffer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(JsonConvert.SerializeObject(processor.TasksByUserWithShortNames(buffer),
                Formatting.Indented));
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Third request: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Enter value of UserId");
            buffer = Convert.ToInt32(Console.ReadLine());
            Console.ForegroundColor = default;
            Console.WriteLine(JsonConvert.SerializeObject(processor.TasksFinishedInCurrentYear(buffer),
                Formatting.Indented));
            Console.WriteLine("Fourth request: ");
            Console.ForegroundColor = default;
            Console.WriteLine(JsonConvert.SerializeObject(processor.TeamInfos(),
                Formatting.Indented));
            Console.WriteLine("Fifth request: ");
            Console.ForegroundColor = default;
            Console.WriteLine(JsonConvert.SerializeObject(processor.SortedUsers(),
                Formatting.Indented));
            Console.WriteLine("Sixth request: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Enter value of UserId");
            Console.ForegroundColor = default;
            buffer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(JsonConvert.SerializeObject(processor.UserInfo(buffer),
                Formatting.Indented));
            Console.WriteLine("Seventh request: ");
            Console.ForegroundColor = default;
            Console.WriteLine(JsonConvert.SerializeObject(processor.ProjectInfos(),
                Formatting.Indented));
        }
    }
}