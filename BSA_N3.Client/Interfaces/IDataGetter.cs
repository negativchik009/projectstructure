﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BSA_N3.Client.Models;
using Task = BSA_N3.Client.Models.Task;

namespace BSA_N3.Client.Interfaces
{
    public interface IDataGetter
    {
        public Task<Dictionary<Project, int>> TasksForUserByProject(int userId); // selection 1
        public Task<IEnumerable<Task>> TasksByUserWithShortNames(int userId); // selection 2
        public Task<IEnumerable<TaskIdNamePair>> TasksFinishedInCurrentYear(int userId); // selection 3
        public Task<IEnumerable<TeamInfo>> TeamInfos(); // selection 4
        public Task<IEnumerable<User>> SortedUsers(); // selection 5
        public Task<UserInfo> UserInfo(int userId); // selection 6
        public Task<IEnumerable<ProjectInfo>> ProjectInfos(); // selection 7
    }
}